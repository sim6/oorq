# -*- coding: utf-8 -*-
{
    "name": "oorq",
    "version": "1.7.3",
    "depends": ["base"],
    "author": "Eduard Carreras",
    "category": "Base",
    "description": """
    This module provide :
      * Use python-rq (Redis Queue) to manage jobs
    """,
    "init_xml": [],
    'update_xml': ['oorq_view.xml'],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
